
export PATH=${PWD}/bin:$PATH
export FABRIC_CFG_PATH=${PWD}/config
cryptogen generate --config=./crypto-config.yaml --output="crypto-config"
configtxgen -outputBlock ./artifacts/genesis.block -profile TwoOrgsGenesis -channelID mygenesis
mdkir temp
configtxgen -inspectBlock ./artifacts/genesis.block >./temp/genesis.json
configtxgen -outputCreateChannelTx ./artifacts/sys-channel.tx -profile TwoOrgsChannel -channelID mychannel
configtxgen -inspectChannelCreateTx ./artifacts/sys-channel.tx >./temp/sys-channel.json
configtxgen -outputAnchorPeersUpdate ./artifacts/Org1Anchor.tx -profile TwoOrgsChannel -channelID mychannel -asOrg Org1
configtxgen -outputAnchorPeersUpdate ./artifacts/Org2Anchor.tx -profile TwoOrgsChannel -channelID mychannel -asOrg Org2
configtxgen -inspectChannelCreateTx ./artifacts/Org1Anchor.tx >./temp/Org1Anchor.json
configtxgen -inspectChannelCreateTx ./artifacts/Org2Anchor.tx >./temp/Org2Anchor.json
