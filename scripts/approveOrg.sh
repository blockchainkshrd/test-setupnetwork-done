export PACKAGE_ID=mycc:6984be3320975d45ebd50aae6c71a74d33fdca4ef7e7d3b99b9487206f82ca23

export PATH=${PWD}/bin:$PATH
export FABRIC_CFG_PATH=${PWD}/config/
export CORE_PEER_ID="Org1MSP"
export CORE_PEER_LOCALMSPID="Org1MSP"
export CORE_PEER_ADDRESS=localhost:7051
export CORE_PEER_MSPCONFIGPATH=${PWD}/crypto-config/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp
peer lifecycle chaincode install mycc.tar.gz --peerAddresses localhost:7051
peer lifecycle chaincode queryinstalled --peerAddresses localhost:7051
peer lifecycle chaincode approveformyorg  -o localhost:7050 --channelID mychannel --name mycc --version --init-required --package-id mycc:b68b9e9491a28b2febc971a43417f60aec4be2d98ba8c48ace9378bc95e6fc1d --sequence 1 



export PATH=${PWD}/bin:$PATH
export FABRIC_CFG_PATH=${PWD}/config/
export CORE_PEER_ID="Org2MSP"
export CORE_PEER_LOCALMSPID="Org2MSP"
export CORE_PEER_ADDRESS=localhost:7053
export CORE_PEER_MSPCONFIGPATH=${PWD}/crypto-config/peerOrganizations/org2.example.com/users/Admin@org2.example.com/msp
peer lifecycle chaincode install mycc.tar.gz --peerAddresses localhost:7053
peer lifecycle chaincode queryinstalled --peerAddresses localhost:7053
peer lifecycle chaincode approveformyorg  -o localhost:7050 --channelID mychannel --name mycc --version 1 --init-required --package-id mycc:b68b9e9491a28b2febc971a43417f60aec4be2d98ba8c48ace9378bc95e6fc1d --sequence 1 