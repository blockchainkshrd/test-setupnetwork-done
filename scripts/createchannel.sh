
export PATH=${PWD}/bin:$PATH
export FABRIC_CFG_PATH=$PWD/config
export CORE_PEER_MSPCONFIGPATH=${PWD}/crypto-config/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp
peer channel create -o localhost:7050 -c mychannel -f artifacts/sys-channel.tx --outputBlock ./artifacts/channel1.block