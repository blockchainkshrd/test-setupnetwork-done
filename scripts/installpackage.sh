export PATH=${PWD}/bin:$PATH
export FABRIC_CFG_PATH=${PWD}/config/
export CORE_PEER_ID="Org1MSP"
export CORE_PEER_LOCALMSPID="Org1MSP"
export CORE_PEER_ADDRESS=localhost:7051
export CORE_PEER_MSPCONFIGPATH=${PWD}/crypto-config/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp
peer lifecycle chaincode package mycc.tar.gz --path ./chaincode --lang golang --label mycc
peer lifecycle chaincode install mycc.tar.gz --peerAddresses localhost:7051
peer lifecycle chaincode queryinstalled --peerAddresses localhost:7051



# mycc:b68b9e9491a28b2febc971a43417f60aec4be2d98ba8c48ace9378bc95e6fc1d,