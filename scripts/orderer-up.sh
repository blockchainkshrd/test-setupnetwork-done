export PATH=${PWD}/bin:$PATH
export ORDERER_FILELEDGER_LOCATION=../ledger/
export FABRIC_LOGGING_SPEC=INFO
export FABRIC_CFG_PATH=${PWD}/config
export ORDERER_GENERAL_LISTENADDRESS=127.0.0.1
export ORDERER_GENERAL_GENESISMETHOD=file
export ORDERER_GENERAL_GENESISFILE=../artifacts/genesis.block
export ORDERER_GENERAL_LOCALMSPID=OrdererMSP
export ORDERER_GENERAL_LOCALMSPDIR=../crypto-config/ordererOrganizations/example.com/orderers/orderer.example.com/msp
orderer