
export PATH=${PWD}/bin:$PATH
export FABRIC_CFG_PATH=${PWD}/config/
export CORE_PEER_ID="Org1MSP"
export CORE_PEER_LOCALMSPID="Org1MSP"
export CORE_PEER_ADDRESS=localhost:7051
export CORE_PEER_MSPCONFIGPATH=${PWD}/crypto-config/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp
peer lifecycle chaincode checkcommitreadiness -o localhost:7050 --channelID mychannel --name mycc --version 1 --init-required --sequence 1
peer lifecycle chaincode commit -o localhost:7050 --channelID mychannel --name mycc --version 1 --sequence 1 --init-required --peerAddresses localhost:7051 --peerAddresses localhost:7053


# //init
# peer chaincode invoke -o localhost:7050 --isInit -C mychannel -n mycc --peerAddresses localhost:7051 --peerAddresses localhost:7053 -c '{"Args":["InitLedger",""]}' --waitForEvent

'{"Args":["CreateCar","CAR100","USA","Pr","white","me"]}'

#Insert
# peer chaincode invoke -o localhost:7050 -C mychannel -n mycc --peerAddresses localhost:7051 --peerAddresses localhost:7053 -c '{"Args":["CreateCar","CAR100","USA","Pr","white","me"]}' --waitForEvent


# peer chaincode query -C mychannel -n mycc -c '{"Args":["GetAllAssets","",""]}'

# peer chaincode instantiate -o orderer.example.com:7050 -C mychannel -n mycc -v 1.0 -c '{"Args":["init","a","100","b","200"]}' -P "AND ('Org1MSP.peer','Org2MSP.peer')"

# peer chaincode upgrade -o localhost:7050 -C mychannel -n mycc -v 1.2 -c '{"Args":["Testcode"]}


# chekck 

# peer lifecycle chaincode checkcommitreadiness --channelID mychannel --name mycc --version 1 --sequence 1 --output json --init-required