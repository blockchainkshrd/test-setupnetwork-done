
export PATH=${PWD}/bin:$PATH
export FABRIC_CFG_PATH=${PWD}/config/




setGlobalsForPeer0Org1(){
    export CORE_PEER_LOCALMSPID="Org1MSP"
    export CORE_PEER_ID="Org1MSP"
    export CORE_PEER_MSPCONFIGPATH=${PWD}/crypto-config/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp
    export CORE_PEER_ADDRESS=localhost:7051
}
setGlobalsForPeer0Org2(){
    export CORE_PEER_LOCALMSPID="Org2MSP"
    export CORE_PEER_ID="Org2MSP"
    export CORE_PEER_MSPCONFIGPATH=${PWD}/crypto-config/peerOrganizations/org2.example.com/users/Admin@org2.example.com/msp
    export CORE_PEER_ADDRESS=localhost:7053
    
}
updateAnchorPeers1(){
    setGlobalsForPeer0Org1
    peer channel update -o localhost:7050 -c mychannel -f ./artifacts/Org1Anchor.tx 
}

updateAnchorPeers2(){
    setGlobalsForPeer0Org2
    peer channel update -o localhost:7050 -c mychannel -f ./artifacts/Org2Anchor.tx 
}


updateAnchorPeers1
updateAnchorPeers2